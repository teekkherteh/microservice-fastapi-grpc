# sample-grpc-fast-api-app

This sample project is to show the usage of gRPC.
Refer `get_book` function in `book_service/app/api/services` for the actual usage (provided with normal REST API and gRPC sample code)

## Getting started

Navigate to the project directory and get hands dirty.

### Create virtual environment

```bash
python -m venv venv
```

or

```bash
python -m pip install virtualenv
virtualenv venv
```

### Activate the virtual environment

```bash
source venv/Scripts/activate
```

### Install gRPC & all the modules

```bash
python -m pip install grpcio grpcio-tools
```

```bash
pip install -r requirements.txt
```

### Install PostgreSQL

Download [PostgreSQL](https://www.postgresql.org/download/). Tables will be created once you start the applications. We will use authors and books table to show gRPC usecase.
Create extension with default uuid-ossp module to generate universally unique identifiers (UUIDs).

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

### Start Fast API server

#### Book service

```bash
cd solutions_services/book_service
python app/main.py
```

#### Author service

Open a new terminal for author service.

```bash
cd solutions_services/author_service
python app/main.py
```

`main.py` will use `uvicorn` to start FastAPI server with port 8001 and 8002 respectively. Try it out in `http://127.0.0.1:8001/api/v1/books/docs#` and `http://127.0.0.1:8002/api/v1/authors/docs#`

Kindly create a author record, following with a book record with POST method.

## Run a gRPC application

### Run gRPC server

Open a new terminal for author service grpc server.

```bash
cd solutions_services/author_service
python app/server.py
```

### Run gRPC client

Open a another terminal for author service grpc client if you want to try it within the same application.

```bash
cd solutions_services/author_service
python app/client.py
```

Congratulations! You’ve just run a client-server application with gRPC.

Check on the response in client terminal and you will see the author details you filled in.

For the sample function created in book service, we will use

```bash
cd solutions_services/author_service
python app/client.py
```

### Generate gRPC code

update the gRPC code used by our application to use the new service definition.

```bash
cd app

python -m grpc_tools.protoc --proto_path=.  --python_out=. --grpc_python_out=. user/user.proto
```

## Support

==Not all endpoint is error proofing for testing as this is POC application.== Kindly modify the code to make it works.

This project can be renamed to DAI Solutions with frontend code included when we kickstart revamp initiative.

## Contributing

Thanks for everyone contribution. Comment and feedback are welcome.

## License

Copyright 2023 Eggplant
