import uuid
from api.models import AuthorIn, AuthorOut, AuthorUpdate
from api.db import authors, database


async def add_author(payload: AuthorIn):
    query = authors.insert().returning(authors).values(**payload.dict())
    return await database.execute(query=query)

async def get_author(id: uuid.UUID):
    query = authors.select(authors.c.id==uuid.UUID(id))
    return await database.fetch_one(query=query)

async def get_all_authors():
    query = authors.select()
    return await database.fetch_all(query=query)