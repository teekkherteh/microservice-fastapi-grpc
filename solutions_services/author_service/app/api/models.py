import datetime
import time
import uuid
from pydantic import BaseModel
from typing import List, Optional

class AuthorIn(BaseModel):
    name: str
    nationality: Optional[str] = None


class AuthorOut(AuthorIn):
    id: uuid.UUID


class AuthorUpdate(AuthorIn):
    name: Optional[str] = None
