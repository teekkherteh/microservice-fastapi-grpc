from fastapi import APIRouter, HTTPException
from typing import List

from api.models import AuthorOut, AuthorIn, AuthorUpdate
import api.db_manager as db_manager

authors = APIRouter()

@authors.post('/', response_model=AuthorOut, status_code=201)
async def create_author(payload: AuthorIn):
    author_id = await db_manager.add_author(payload)

    print(author_id)
    response = {
        'id': author_id,
        **payload.dict()
    }

    return response

@authors.get('/{id}', response_model=AuthorOut)
async def get_author(id: str):
    author = await db_manager.get_author(id)
    if not author:
        raise HTTPException(status_code=404, detail="Author not found")
    return author

@authors.get('/', response_model=List[AuthorOut])
async def get_authors():
    return await db_manager.get_all_authors()
