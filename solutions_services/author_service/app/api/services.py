from fastapi import APIRouter, HTTPException
from typing import List

from api.models import AuthorOut, AuthorIn, AuthorUpdate
import api.db_manager as db_manager


async def create_author(payload: AuthorIn):
     return await db_manager.add_author(payload)

async def get_author(id: str):
    return await db_manager.get_author(id)

async def get_authors():
    return await db_manager.get_all_authors()
