import sqlalchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, String
from sqlalchemy import (Column, MetaData, String, Table,
                        create_engine)

from databases import Database

DATABASE_URI = 'postgresql://postgres:admin@localhost:5434/postgres'

engine = create_engine(DATABASE_URI)
metadata = MetaData()

authors = Table(
    'authors',
    metadata,
    Column('id', UUID(), server_default=sqlalchemy.text("uuid_generate_v4()"), primary_key=True),
    Column('name', String(50)),
    Column('nationality', String(20)),
)

database = Database(DATABASE_URI)