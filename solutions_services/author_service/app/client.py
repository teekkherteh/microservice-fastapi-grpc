from __future__ import print_function
import asyncio

import logging
import sys

import path
import grpc

# directory reach
directory = path.Path(__file__).abspath()

# setting path
sys.path.append(directory.parent.parent.parent)
import protobuf.author_pb2 as author_pb2
import protobuf.author_pb2_grpc as author_pb2_grpc
import api.services as services

# sample grpc client
async def run():
    async with grpc.aio.insecure_channel('localhost:50001') as channel:
        stub = author_pb2_grpc.AuthorServiceStub(channel)
        response = await stub.GetAuthor(author_pb2.AuthorRequest(id='30d46762-1e1f-4cab-84e9-9387faa94461'))
    print(response)


if __name__ == '__main__':
    logging.basicConfig()
    asyncio.run(run())
