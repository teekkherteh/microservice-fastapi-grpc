
import asyncio
from concurrent import futures
import logging
import sys
import path
from api.db import database

import grpc

# directory reach
directory = path.Path(__file__).abspath()

# setting path
sys.path.append(directory.parent.parent.parent)
import protobuf.author_pb2 as author_pb2
import protobuf.author_pb2_grpc as author_pb2_grpc
import api.services as services

# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []

class AuthorService(author_pb2_grpc.AuthorServiceServicer):

    async def GetAuthor(self, request, context):
        print('start')
        author = await services.get_author(request.id)

        return author_pb2.AuthorReply(
                id=str(author['id']),
                name=author['name'],
                nationality=author['nationality']
            )

async def serve() -> None:
    port = '50001'
    server = grpc.aio.server(futures.ThreadPoolExecutor(max_workers=10))
    author_pb2_grpc.add_AuthorServiceServicer_to_server(AuthorService(), server)
    server.add_insecure_port('[::]:' + port)
    print("Server started, listening on " + port)
    await server.start()
    await database.connect()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 5 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await database.disconnect()
        await server.stop(5)
    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()



# if __name__ == '__main__':
#     logging.basicConfig()
#     asyncio.run(serve())

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()