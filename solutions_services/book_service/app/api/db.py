import sqlalchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import ARRAY, Column, String
from sqlalchemy import (Column, MetaData, String, Table,
                        create_engine)
from databases import Database
import sqlalchemy

DATABASE_URI = 'postgresql://postgres:admin@localhost:5434/postgres'

engine = create_engine(DATABASE_URI)
metadata = MetaData()

books = Table(
    'books',
    metadata,
    Column('id', UUID(), server_default=sqlalchemy.text("uuid_generate_v4()"), primary_key=True),
    Column('title', String(50)),
    Column('desc', String(250)),
    Column('genres', ARRAY(String)),
    Column('author_id', ARRAY(UUID()))
)

database = Database(DATABASE_URI)