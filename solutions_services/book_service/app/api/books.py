from datetime import datetime
from typing import List

from fastapi import APIRouter, HTTPException
from google.protobuf import json_format

from api.models import BookOut, BookIn, BookUpdate
import api.services as services

books = APIRouter()

@books.post('/', response_model=BookOut, status_code=201)
async def create_book(payload: BookIn):
    for author_id in payload.author_id:
        if not services.is_author_present(author_id):
            raise HTTPException(status_code=404, detail=f"Author with given id:{author_id} not found")

    book_id = await services.create_book(payload)
    response = {
        'id': book_id,
        **payload.dict()
    }

    return response

@books.get('/')
async def get_books():
    st = datetime.now()
    print("Start date and time is:", st)

    books = await services.get_books()

    et = datetime.now()
    print("End date and time is:", et)
    print("Total time taken is:", datetime.timestamp(et) - datetime.timestamp(st))
    return books

@books.get('/{id}/')
async def get_book(id: str):
    st = datetime.now()
    print("Start date and time is:", st)
    book = await services.get_book(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")

    et = datetime.now()
    print("End date and time is:", et)
    print("Total time taken is:", datetime.timestamp(et) - datetime.timestamp(st))

    return book



@books.put('/{id}/', response_model=BookOut)
async def update_book(id: int, payload: BookUpdate):
    book = await services.get_book(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")

    update_data = payload.dict(exclude_unset=True)

    if 'authors_id' in update_data:
        for author_id in payload.author_id:
            if not services.is_author_present(author_id):
                raise HTTPException(status_code=404, detail=f"Author with given id:{author_id} not found")

    book_in_db = BookIn(**book)

    updated_book = book_in_db.copy(update=update_data)

    return await services.update_book(id, updated_book)

@books.delete('/{id}/', response_model=None)
async def delete_book(id: str):
    book = await services.get_book(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    return await services.delete_book(id)
