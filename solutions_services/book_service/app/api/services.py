import datetime
import os
import sys
import uuid
from fastapi import HTTPException
from google.protobuf import json_format
import grpc
import httpx
import path

from api.models import BookIn, BookUpdate
import api.db_manager as db_manager

# directory reach
directory = path.Path(__file__).abspath()

# setting path
sys.path.append(directory.parent.parent.parent.parent)
print(directory.parent.parent.parent)
from protobuf import author_pb2, author_pb2_grpc

CAST_SERVICE_HOST_URL = 'http://localhost:8002/api/v1/authors/'

async def create_book(payload: BookIn):
    return await db_manager.add_book(payload)

async def get_books():
    # return await db_manager.get_all_books()

    # return book with author details instead of author id
    books_record = await db_manager.get_all_books()
    books = []
    for book in books_record:
        author_list = []

        for author_id in book['author_id']:
            # REST API
            author_details = await get_author_details_rest(author_id)
            author_list.append(author_details)

            # gRPC
            # author_details = await get_author_details_grpc(str(author_id))
            # author_list.append(json_format.MessageToDict(author_details))

            books.append(
                {
                "title": book["title"],
                "desc": book["desc"],
                "genres": book["genres"],
                "authors": author_list,
                "id": book["id"]
                }
            )

    return books


async def get_book(id: str):
    book = await get_book(id)
    if not book:
        return {book}

    author_list = []

    for author_id in book['author_id']:
        # REST API
        author_details = await get_author_details_rest(author_id)
        author_list.append(author_details)

        # gRPC
        # author_details = await get_author_details_grpc(str(author_id))
        # author_list.append(json_format.MessageToDict(author_details))


    return {
    "title": book["title"],
    "desc": book["desc"],
    "genres": book["genres"],
    "authors": author_list,
    "id": book["id"]
    }


async def update_book(id: int, payload: BookUpdate):
    book = await db_manager.get_book(id)
    if not book:
        return book

    update_data = payload.dict(exclude_unset=True)

    if 'authors_id' in update_data:
        for author_id in payload.author_id:
            if not is_author_present(author_id):
                raise HTTPException(status_code=404, detail=f"Author with given id:{author_id} not found")

    book_in_db = BookIn(**book)

    updated_book = book_in_db.copy(update=update_data)

    return await db_manager.update_book(id, updated_book)

async def delete_book(id: str):
    return await db_manager.delete_book(id)

async def get_book(id: str):
    return await db_manager.get_book(uuid.UUID(id))

async def is_author_present(author_id: str):
    url = os.environ.get('CAST_SERVICE_HOST_URL') or CAST_SERVICE_HOST_URL
    r = httpx.get(f'{url}{author_id}')
    return True if r.status_code == 200 else False

async def get_author_details_rest(author_id: str):
    url = os.environ.get('CAST_SERVICE_HOST_URL') or CAST_SERVICE_HOST_URL
    async with httpx.AsyncClient() as client:
        response = await client.get(f'{url}{author_id}')
        return response.json()

async def get_author_details_grpc(author_id: str):
    async with grpc.aio.insecure_channel('localhost:50001') as channel:
        stub = author_pb2_grpc.AuthorServiceStub(channel)
        response = await stub.GetAuthor(author_pb2.AuthorRequest(id=author_id))
    return response