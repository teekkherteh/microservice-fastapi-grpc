import uuid
from pydantic import BaseModel
from typing import List, Optional

class BookIn(BaseModel):
    title: str
    desc: str
    genres: List[str]
    author_id: List[uuid.UUID]


class BookOut(BookIn):
    id: uuid.UUID


class BookUpdate(BookIn):
    title: Optional[str] = None
    desc: Optional[str] = None
    genres: Optional[List[str]] = None
    author_id: Optional[List[uuid.UUID]] = None